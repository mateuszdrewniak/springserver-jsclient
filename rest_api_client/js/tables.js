const tableName = params.get('table')
const page = Number.parseInt(params.get('page')) || 0
const limit = params.get('limit') || 5
const tableHeadRow = document.querySelector('#table-head tr')
const tableBody = document.getElementById('table-body')
const pagination = document.getElementById('pagination')
const tableSize = document.getElementById('table-size')
const tablePages = document.getElementById('table-pages')
const perPage = document.getElementById('per-page')
const tablePage = document.getElementById('table-page')
const createBtn = document.getElementById('create-btn')
const newBtn = document.getElementById('new-btn')
const createForm = document.getElementById('create-form')
var requestResponse
var relatedTables = {}

function createNewRow() {
  let formData = JSON.stringify(Object.fromEntries(new FormData(createForm)))
  let positive = (response) => {
    displayNotice('Created!')
    setTimeout(() => { window.location.href = `./tables.html?table=${tableName}&page=${page}&limit=${limit}` }, 1000)
  }

  let negative = (response) => {
    displayAlert('Communication Error!')
    response = JSON.parse(response)
    console.log(response)
  }
  sendRequest({ path: `${apiUrl}${tableName}`, method: 'POST', contentType: 'application/json', payload: formData, positive: positive, negative: negative })
}

function loadTable() {
  let positive = (response) => {
    requestResponse = JSON.parse(response)
    console.log(requestResponse)
    renderTable()
  }

  let negative = (response) => {
    displayAlert('Communication Error!')
    response = JSON.parse(response)
    console.log(response)
  }
  sendRequest({ path: `${apiUrl}${tableName}?page=${page}&size=${limit}`, method: 'GET', positive: positive, negative: negative })
}

function renderTable() {
  tableSize.innerText = requestResponse.totalElements
  tablePages.innerText = requestResponse.totalPages
  tablePage.innerText = page

  let headKey = document.querySelector('#table-head th').cloneNode(true)
  let rowKey = document.querySelector('#table-body td').cloneNode(true)
  for(let node of document.querySelectorAll('#table-head th')) node.remove()

  let headKeys = Object.keys(requestResponse.content[0])
  for(let index in headKeys) {
    let key = headKeys[index]
    let newHeadElement = headKey.cloneNode()
    newHeadElement.append(document.createTextNode(key))
    if(key.endsWith('Id')) {
      let relatedTable = camelToSnakeCase(key).replace('_id','s')
      relatedTables[index] = relatedTable
    }
    tableHeadRow.append(newHeadElement)
  }

  document.querySelector('#table-body tr').remove()

  for(let row of requestResponse.content) {
    let tr = document.createElement('tr')
    
    let values = Object.values(row)
    for(let index in values) {
      let value = values[index]
      let td = document.createElement('td')

      if(relatedTables[index] != null && value != 0) {
        let a = document.createElement('a')
        a.href = `./rows.html?table=${relatedTables[index]}&id=${value}`
        a.append(document.createTextNode(value))
        td.append(a)
      } else {
        td.append(document.createTextNode(value))
      }
      tr.append(td)
      tr.addEventListener('click', () => {
        window.location.href = `./rows.html?table=${tableName}&id=${row['id']}`
      })
    }

    tableBody.append(tr)
  }

  for(let index in headKeys) {
    let key = headKeys[index]
    if(key == 'createdAt' || key == 'id') continue
    let input = document.createElement('input')
    let label = document.createElement('label')
    input.type = 'text'
    input.name = key
    input.classList.add('form-control')
    input.classList.add('form-label')
    label.append(document.createTextNode(key))

    createForm.append(label)
    createForm.append(input)
  }
  
  if(requestResponse.totalPages > 1) {
    let pageItem
    let pageItemKey = document.querySelector('#pagination li').cloneNode(true)
    for(let node of document.querySelectorAll('#pagination li')) node.remove()

    if(page > 0) {
      pageItem = pageItemKey.cloneNode(true)
      pageItem.firstElementChild.innerText = 'Previous'
      pageItem.firstElementChild.href = `./tables.html?table=${tableName}&page=${page-1}&limit=${limit}`
      pagination.append(pageItem)

      pageItem = pageItem.cloneNode(true)
      pageItem.firstElementChild.innerText = page - 1
      pagination.append(pageItem)
    }

    pageItem = pageItemKey.cloneNode(true)
    pageItem.firstElementChild.innerText = page
    pageItem.firstElementChild.href = `./tables.html?table=${tableName}&page=${page}&limit=${limit}`
    pagination.append(pageItem)

    if(requestResponse.totalPages > (page + 1)) {
      pageItem = pageItemKey.cloneNode(true)
      pageItem.firstElementChild.innerText = page + 1
      pageItem.firstElementChild.href = `./tables.html?table=${tableName}&page=${page+1}&limit=${limit}`
      pagination.append(pageItem)

      pageItem = pageItem.cloneNode(true)
      pageItem.firstElementChild.innerText = 'Next'
      pagination.append(pageItem)
    }
  } else {
    pagination.remove()
  }
}

perPage.innerText = limit
document.title = capitalize(snakeToCamel(tableName))
document.getElementById('table-name').innerText = `${capitalize(snakeToCamel(tableName))} Table`
newBtn.addEventListener('click', () => {
  createForm.classList.toggle('hidden')
})
createBtn.addEventListener('click', createNewRow)
loadTable()
