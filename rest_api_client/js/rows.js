const tableBody = document.getElementById('table-body')
const tableName = params.get('table')
const tableLink = document.getElementById('table-link')
const deleteBtn = document.getElementById('delete-btn')
const editBtn = document.getElementById('edit-btn')
const showEditBtn = document.getElementById('show-edit-btn')
const editForm = document.getElementById('edit-form')
const id = params.get('id')
var requestResponse

function editRow() {
  let formData = JSON.stringify(Object.fromEntries(new FormData(editForm)))
  let positive = (response) => {
    displayNotice('Edited!')
    setTimeout(() => { window.location.href = window.location.href }, 1000)
  }

  let negative = (response) => {
    displayAlert('Communication Error!')
    response = JSON.parse(response)
    console.log(response)
  }
  sendRequest({ path: `${apiUrl}${tableName}/${id}`, method: 'PUT', contentType: 'application/json', payload: formData, positive: positive, negative: negative })
}

function deleteRow() {
  let positive = (response) => {
    displayNotice('Deleted!')
    setTimeout(() => { window.location.href = `./tables.html?table=${tableName}` }, 1000)
  }

  let negative = (response) => {
    displayAlert('Communication Error!')
    console.log(response)
    response = JSON.parse(response)
    console.log(response)
  }
  sendRequest({ path: `${apiUrl}${tableName}/${id}`, method: 'DELETE', positive: positive, negative: negative })
}

function loadRow() {
  let positive = (response) => {
    requestResponse = JSON.parse(response)
    console.log(requestResponse)
    renderRow()
  }

  let negative = (response) => {
    displayAlert('Communication Error!')
    response = JSON.parse(response)
    console.log(response)
  }
  sendRequest({ path: `${apiUrl}${tableName}/${id}`, method: 'GET', positive: positive, negative: negative })
}

function renderRow() {
  document.querySelector('#table-body tr').remove()

  let keys = Object.keys(requestResponse)
  for(let key of keys) {
    let tr = document.createElement('tr')
    
    let value = requestResponse[key]
    let keyTd = document.createElement('td')
    let valueTd = document.createElement('td')
    let input
    let label

    input = document.createElement('input')
    label = document.createElement('label')
    input.type = 'text'
    input.name = key
    input.value = value
    input.classList.add('form-control')
    input.classList.add('form-label')
    label.append(document.createTextNode(key))

    if(key == 'createdAt' || key == 'id') {
      input.classList.add('hidden')
      label.classList.add('hidden')
    }

    editForm.append(label)
    editForm.append(input)

    keyTd.append(document.createTextNode(key))
    if(key.endsWith('Id') && value != 0) {
      let relatedTable = camelToSnakeCase(key).replace('_id','s')
      let a = document.createElement('a')
      a.href = `./rows.html?table=${relatedTable}&id=${value}`
      a.append(document.createTextNode(value))
      valueTd.append(a)
    } else {
      valueTd.append(document.createTextNode(value))
    }
    tr.append(keyTd)
    tr.append(valueTd)

    tableBody.append(tr)
  }
}

showEditBtn.addEventListener('click', () => {
  editForm.classList.toggle('hidden')
})
deleteBtn.addEventListener('click', deleteRow)
tableLink.href = `./tables.html?table=${tableName}`
document.title = `${capitalize(snakeToCamel(tableName))} Row`
document.getElementById('table-name').innerText = `${capitalize(tableName)} Row`
editBtn.addEventListener('click', editRow)
loadRow()
