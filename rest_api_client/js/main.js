const apiUrl = 'http://localhost:8081/'
const info = document.querySelector('#info')
const params = new URLSearchParams(window.location.search)

const camelToSnakeCase = str => str.replace(/[A-Z]/g, letter => `_${letter.toLowerCase()}`)
const snakeToCamel = (str) => str.replace(
  /([-_][a-z])/g,
  (group) => group.toUpperCase()
                  .replace('-', '')
                  .replace('_', '')
)

function sendRequest({ path, payload = null, contentType = null, positive = ()=>{}, negative = ()=>{}, method = 'POST'} = {}) {
  let xhr = new XMLHttpRequest()
  xhr.open(method, path, true)
  if(contentType != null) xhr.setRequestHeader("Content-Type", contentType)
  xhr.onreadystatechange = () => {
    if (xhr.readyState === 4) {
      if(xhr.status === 200 || xhr.status === 201 || xhr.status === 204) {
        // on ready state change
        positive(xhr.response)
      } else {
        negative(xhr.response)
      }
    }
  }
  xhr.send(payload)
}

function capitalize(string) {
  return string[0].toUpperCase() + string.slice(1)
}

function displayInfo({ message, type = 'notice', displayTime = 1000 }) {
  info.classList.add(type)
  info.firstElementChild.innerHTML = message

  info.classList.remove('hidden')
  
  info.style.top = '100px'
  setTimeout(() => {
    info.style.top = '0'
    setTimeout(() => {
      info.classList.add('hidden')
      info.classList.remove(type)
    }, 500)
  }, displayTime)
}

function displayNotice(message, displayTime = 1500) {
  displayInfo({ message: message, displayTime: displayTime })
}

function displayAlert(message, displayTime = 1500) {
  displayInfo({ message: message, type: 'alert', displayTime: displayTime })
}