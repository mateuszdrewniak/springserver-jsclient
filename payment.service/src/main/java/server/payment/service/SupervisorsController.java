package server.payment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(path="/supervisors")
public class SupervisorsController {
    @Autowired
    private SupervisorRepository supervisorRepository;

    @PostMapping
    public ResponseEntity<?> addNewSupervisor(@RequestBody Supervisor newSupervisor) {
        supervisorRepository.save(newSupervisor);
        newSupervisor.setForeignKeys();
        return new ResponseEntity<>(newSupervisor, HttpStatus.CREATED);
    }

    @GetMapping
    public Page<Supervisor> getPaginatedSupervisors(Pageable pageable) {
        Page<Supervisor> supervisors = supervisorRepository.findAll(pageable);
        for (Supervisor s : supervisors) s.setForeignKeys();
        return supervisors;
    }

    @GetMapping(path="/{supervisorId}")
    public ResponseEntity<?> getSupervisor(@PathVariable Integer supervisorId) {
        Supervisor supervisor = supervisorRepository.findById(supervisorId).orElse(null);
        if(supervisor == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        supervisor.setForeignKeys();

        return ResponseEntity.status(HttpStatus.OK).body(supervisor);
    }

    @PutMapping(path="/{supervisorId}")
    public ResponseEntity<?> updateSupervisor(@PathVariable Integer supervisorId, @RequestBody Supervisor newSupervisor) {
        Supervisor supervisor = supervisorRepository.findById(supervisorId).orElse(null);
        if(supervisor == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);

        newSupervisor.setId(supervisor.getId());
        supervisorRepository.save(newSupervisor);
        newSupervisor.setForeignKeys();

        return ResponseEntity.status(HttpStatus.OK).body(newSupervisor);
    }

    @DeleteMapping(path="/{supervisorId}")
    public ResponseEntity<?> deleteSupervisor(@PathVariable Integer supervisorId) {
        Supervisor supervisor = supervisorRepository.findById(supervisorId).orElse(null);
        if(supervisor == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        supervisorRepository.deleteById(supervisorId);

        return ResponseEntity.noContent().build();
    }

    @DeleteMapping
    public ResponseEntity<?> deleteSupervisors() {
        supervisorRepository.deleteAll();
        return ResponseEntity.noContent().build();
    }
}