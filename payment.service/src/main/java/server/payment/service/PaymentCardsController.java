package server.payment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(path="/payment_cards")
public class PaymentCardsController {
    @Autowired
    private PaymentCardRepository paymentCardRepository;
    @Autowired
    private ClientRepository clientRepository;

    @PostMapping
    public ResponseEntity<?> addNewPaymentCard (@RequestBody PaymentCard newPaymentCard) {
        Client client = clientRepository.findById(newPaymentCard.getClientId()).orElse(null);
        if(newPaymentCard.getClientId() != 0 && client == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        if(client != null && client.getPaymentCard() != null)
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);

        newPaymentCard.setClient(client);
        newPaymentCard.setForeignKeys();

        paymentCardRepository.save(newPaymentCard);
        return new ResponseEntity<>(newPaymentCard, HttpStatus.CREATED);
    }

    @GetMapping
    public Page<PaymentCard> getPaginatedPaymentCards(Pageable pageable) {
        Page<PaymentCard> paymentCards = paymentCardRepository.findAll(pageable);
        for (PaymentCard c : paymentCards) c.setForeignKeys();
        return paymentCards;
    }

    @GetMapping(path="/{paymentCardId}")
    public ResponseEntity<?> getPaymentCard(@PathVariable Integer paymentCardId) {
        PaymentCard paymentCard = paymentCardRepository.findById(paymentCardId).orElse(null);
        if(paymentCard == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        paymentCard.setForeignKeys();

        return ResponseEntity.status(HttpStatus.OK).body(paymentCard);
    }

    @PutMapping(path="/{paymentCardId}")
    public ResponseEntity<?> updatePaymentCard(@PathVariable Integer paymentCardId, @RequestBody PaymentCard newPaymentCard) {
        PaymentCard paymentCard = paymentCardRepository.findById(paymentCardId).orElse(null);
        if(paymentCard == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);

        newPaymentCard.setId(paymentCard.getId());
        Client client = clientRepository.findById(newPaymentCard.getClientId()).orElse(null);

        if(newPaymentCard.getClientId() != 0 && client == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        if(client != null && client.getPaymentCard() != null)
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);

        newPaymentCard.setClient(client);
        paymentCardRepository.save(newPaymentCard);
        newPaymentCard.setForeignKeys();

        return ResponseEntity.status(HttpStatus.OK).body(newPaymentCard);
    }

    @DeleteMapping(path="/{paymentCardId}")
    public ResponseEntity<?> deletePaymentCard(@PathVariable Integer paymentCardId) {
        PaymentCard paymentCard = paymentCardRepository.findById(paymentCardId).orElse(null);
        if(paymentCard == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        paymentCardRepository.deleteById(paymentCardId);

        return ResponseEntity.noContent().build();
    }

    @DeleteMapping
    public ResponseEntity<?> deletePaymentCards() {
        paymentCardRepository.deleteAll();
        return ResponseEntity.noContent().build();
    }
}