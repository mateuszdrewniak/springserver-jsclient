package server.payment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(path="/transactions")
public class TransactionsController {
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private MerchantRepository merchantRepository;

    @PostMapping
    public ResponseEntity<?> addNewTransaction (@RequestBody Transaction newTransaction) {
        Merchant merchant = merchantRepository.findById(newTransaction.getMerchantId()).orElse(null);
        Client client = clientRepository.findById(newTransaction.getClientId()).orElse(null);

        if(newTransaction.getClientId() != 0 && client == null || newTransaction.getMerchantId() != 0 && merchant == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        newTransaction.setClient(client);
        newTransaction.setMerchant(merchant);
        transactionRepository.save(newTransaction);
        newTransaction.setForeignKeys();
        return new ResponseEntity<>(newTransaction, HttpStatus.CREATED);
    }

    @GetMapping
    public Page<Transaction> getPaginatedTransactions(Pageable pageable) {
        Page<Transaction> transactions = transactionRepository.findAll(pageable);
        for (Transaction t : transactions) t.setForeignKeys();
        return transactions;
    }

    @GetMapping(path="/{transactionId}")
    public ResponseEntity<?> getTransaction(@PathVariable Integer transactionId) {
        Transaction transaction = transactionRepository.findById(transactionId).orElse(null);
        if(transaction == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        transaction.setForeignKeys();

        return ResponseEntity.status(HttpStatus.OK).body(transaction);
    }

    @PutMapping(path="/{transactionId}")
    public ResponseEntity<?> updateTransaction(@PathVariable Integer transactionId, @RequestBody Transaction newTransaction) {
        Transaction transaction = transactionRepository.findById(transactionId).orElse(null);
        if(transaction == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);

        Merchant merchant = merchantRepository.findById(newTransaction.getMerchantId()).orElse(null);
        Client client = clientRepository.findById(newTransaction.getClientId()).orElse(null);

        if(newTransaction.getClientId() != 0 && client == null || newTransaction.getMerchantId() != 0 && merchant == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        newTransaction.setClient(client);
        newTransaction.setMerchant(merchant);

        newTransaction.setId(transaction.getId());
        transactionRepository.save(newTransaction);
        newTransaction.setForeignKeys();

        return ResponseEntity.status(HttpStatus.OK).body(newTransaction);
    }

    @DeleteMapping(path="/{transactionId}")
    public ResponseEntity<?> deleteTransaction(@PathVariable Integer transactionId) {
        Transaction transaction = transactionRepository.findById(transactionId).orElse(null);
        if(transaction == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        transactionRepository.deleteById(transactionId);

        return ResponseEntity.noContent().build();
    }

    @DeleteMapping
    public ResponseEntity<?> deleteTransactions() {
        transactionRepository.deleteAll();
        return ResponseEntity.noContent().build();
    }
}