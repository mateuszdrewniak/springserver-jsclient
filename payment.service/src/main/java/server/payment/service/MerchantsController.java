package server.payment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Map;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(path="/merchants")
public class MerchantsController {
    @Autowired
    private MerchantRepository merchantRepository;

    @Autowired
    private SupervisorRepository supervisorRepository;

    @PostMapping
    public ResponseEntity<?> addNewMerchant (@RequestBody Merchant newMerchant) {
        Supervisor supervisor = supervisorRepository.findById(newMerchant.getSupervisorId()).orElse(null);

        if(newMerchant.getSupervisorId() != 0 && supervisor == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        if(supervisor!= null && supervisor.getMerchant() != null)
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);

        newMerchant.setSupervisor(supervisor);
        merchantRepository.save(newMerchant);
        newMerchant.setForeignKeys();
        return new ResponseEntity<>(newMerchant, HttpStatus.CREATED);
    }

    @GetMapping
    public Page<Merchant> getPaginatedMerchants(Pageable pageable) {
        Page<Merchant> merchants = merchantRepository.findAll(pageable);
        for (Merchant m : merchants) m.setForeignKeys();
        return merchants;
    }

    @GetMapping(path="/{merchantId}")
    public ResponseEntity<?> getMerchant(@PathVariable Integer merchantId) {
        Merchant merchant = merchantRepository.findById(merchantId).orElse(null);
        if(merchant == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        merchant.setForeignKeys();

        return ResponseEntity.status(HttpStatus.OK).body(merchant);
    }

    @PutMapping(path="/{merchantId}")
    public ResponseEntity<?> updateMerchant(@PathVariable Integer merchantId, @RequestBody Merchant newMerchant) {
        Merchant merchant = merchantRepository.findById(merchantId).orElse(null);
        if(merchant == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        Supervisor supervisor = supervisorRepository.findById(newMerchant.getSupervisorId()).orElse(null);

        if(newMerchant.getSupervisorId() != 0 && supervisor == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        if(supervisor!= null && supervisor.getMerchant() != null)
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);

        newMerchant.setId(merchant.getId());
        newMerchant.setSupervisor(supervisor);
        merchantRepository.save(newMerchant);
        newMerchant.setForeignKeys();

        return ResponseEntity.status(HttpStatus.OK).body(newMerchant);
    }

    @DeleteMapping(path="/{merchantId}")
    public ResponseEntity<?> deleteMerchant(@PathVariable Integer merchantId) {
        Merchant merchant = merchantRepository.findById(merchantId).orElse(null);
        if(merchant == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        merchantRepository.deleteById(merchantId);

        return ResponseEntity.noContent().build();
    }

    @DeleteMapping
    public ResponseEntity<?> deleteMerchants() {
        merchantRepository.deleteAll();
        return ResponseEntity.noContent().build();
    }
}