package server.payment.service;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SupervisorRepository extends PagingAndSortingRepository<Supervisor, Integer> {

}
