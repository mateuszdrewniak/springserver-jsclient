package server.payment.service;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "merchants", uniqueConstraints = { @UniqueConstraint(columnNames = { "mid" }) })
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Merchant {

    @Id @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "support_email")
    private String supportEmail;

    @Column(name = "support_phone")
    private String supportPhone;

    @Column(name = "city")
    private String city;

    @Column(name = "country_code", nullable = false)
    private String countryCode;

    @Column(name = "mid", nullable = false, unique = true)
    private String mid;

    @Column(name = "created_at", nullable = false)
    private ZonedDateTime createdAt;

    @OneToMany(mappedBy = "merchant", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<Transaction> transactions = new HashSet<Transaction>();

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "supervisor_id", referencedColumnName = "id")
    @JsonIgnore
    private Supervisor supervisor;

    @Transient
    private int supervisorId;

    public Merchant() {
        this.createdAt = ZonedDateTime.now();
    }

    public void setForeignKeys() {
        if(supervisor == null) this.supervisorId = 0;
        else this.supervisorId = supervisor.getId();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSupervisorId() {
        return supervisorId;
    }

    public void setSupervisorId(int supervisorId) {
        this.supervisorId = supervisorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSupportEmail() {
        return supportEmail;
    }

    public void setSupportEmail(String supportEmail) {
        this.supportEmail = supportEmail;
    }

    public String getSupportPhone() {
        return supportPhone;
    }

    public void setSupportPhone(String supportPhone) {
        this.supportPhone = supportPhone;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Set<Transaction> getTransactions() {
        return transactions;
    }

    public Supervisor getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(Supervisor supervisor) {
        this.supervisor = supervisor;
    }
}