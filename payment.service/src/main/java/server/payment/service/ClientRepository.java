package server.payment.service;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import server.payment.service.Client;

@Repository
public interface ClientRepository extends PagingAndSortingRepository<Client, Integer> {

}
