package server.payment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(path="/clients")
public class ClientsController {
    @Autowired
    private ClientRepository clientRepository;

    @PostMapping
    public ResponseEntity<?> addNewClient (@RequestBody Client newClient) {
        clientRepository.save(newClient);
        newClient.setForeignKeys();
        return new ResponseEntity<>(newClient, HttpStatus.CREATED);
    }

    @GetMapping
    public Page<Client> getPaginatedClients(Pageable pageable) {
        Page<Client> clients = clientRepository.findAll(pageable);
        for (Client c : clients) c.setForeignKeys();
        return clients;
    }

    @GetMapping(path="/{clientId}")
    public ResponseEntity<?> getClient(@PathVariable Integer clientId) {
        Client client = clientRepository.findById(clientId).orElse(null);
        if(client == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        client.setForeignKeys();

        return ResponseEntity.status(HttpStatus.OK).body(client);
    }

    @PutMapping(path="/{clientId}")
    public ResponseEntity<?> updateClient(@PathVariable Integer clientId, @RequestBody Client newClient) {
        Client client = clientRepository.findById(clientId).orElse(null);
        if(client == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);

        client.setFirstName(newClient.getFirstName());
        client.setLastName(newClient.getLastName());
        client.setDescription(newClient.getDescription());
        client.setEmail(newClient.getEmail());
        clientRepository.save(client);
        client.setForeignKeys();

        return ResponseEntity.status(HttpStatus.OK).body(client);
    }

    @DeleteMapping(path="/{clientId}")
    public ResponseEntity<?> deleteClient(@PathVariable Integer clientId) {
        Client client = clientRepository.findById(clientId).orElse(null);
        if(client == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        clientRepository.deleteById(clientId);

        return ResponseEntity.noContent().build();
    }

    @DeleteMapping
    public ResponseEntity<?> deleteClients() {
        clientRepository.deleteAll();
        return ResponseEntity.noContent().build();
    }
}